
# coding: utf-8

# # IML Challenge ROOT TMVA Example
# 
# This is a binary classification example for the IML challenge. It implements a full toolchain for loading data files, train multiple classifiers and evaluate them. The used framework is [TMVA](https://root.cern.ch/root-user-guides-and-manuals) and optionally Keras with Theano.

# ## Set up TMVA

# In[1]:

import ROOT

# Set up TMVA factory
output_file = ROOT.TFile.Open('TMVAClassification.root', 'RECREATE')
factory = ROOT.TMVA.Factory('TMVAClassification', output_file,
        '!V:!Silent:Color:!DrawProgressBar:Transformations=I,G:'+\
        'AnalysisType=Classification')


# In[2]:

#ml from IPython.core.extensions import ExtensionManager
##ml ExtensionManager(get_ipython()).load_extension("JsMVA.JsMVAMagic")
#ml %jsmva on


# ## Load data
# 
# **NOTE:** The data loaded here is already preprocessed. Checkout out the script `PREPROCESS_DATA` shipped with this notebook and run it in the terminal with ```sh PREPROCESS_DATA``` (you can do this on lxplus, after enabling the LCG software stack, or directly on a swan terminal).
# 
# Please note that the script only processes a small fraction of the events by default. Edit it if you want to process more. **IMPORTANT** do not use the full dataset if you run on swan, see the challenge README for details. 
# 
# The preprocessing does a zero-padding of the varying number of tracks and towers per event. We need to do this to include the given low-level information because we need a fix number of inputs for most MVA methods. For example, if we want to keep 5 tracks per event but we have only 3 in this event, then we are setting the track information of the 2 missing tracks to zero.

# In[3]:

# NOTE: Check out the `preprocess_data.py` script shipped with this notebook!
data = ROOT.TFile("/eos/user/m/mlisovyi/cernbox/IML_tutorial/data/preprocessed4TMVA_std.root")
#data = ROOT.TFile("preprocessed4TMVA_std.root")
if data == None:
    raise Exception('Have you run the preprocessing? Can not open file: {}'.format(filename))

quarks = data.Get('quarks')
gluons = data.Get('gluons')

# Set up dataloader and book all branches in the trees as input variables
dataloader = ROOT.TMVA.DataLoader('TMVAClassification')
for branch in quarks.GetListOfBranches():
    # NOTE: Here we are restricting the number of variables with rules on the variable name
    if not '_' in branch.GetName():
        print branch.GetName()
        dataloader.AddVariable(branch.GetName())

dataloader.AddTree(quarks, 'quarks', 1.0)
dataloader.AddTree(gluons, 'gluons', 1.0)
dataloader.PrepareTrainingAndTestTree(ROOT.TCut(''),
        # NOTE: Use this config if you run on the full dataset
        #'TrainTestSplit_quarks=0.8:TrainTestSplit_gluons=0.8:'+\
        'nTrain_quarks=20000:nTest_quarks=20000:'+\
        'nTrain_gluons=20000:nTest_gluons=20000:'+\
        'SplitMode=Random:NormMode=NumEvents:!V')
#ml dataloader.AddSignalTree(quarks, 1.0)
#ml dataloader.AddBackgroundTree(gluons, 1.0)
#mldataloader.PrepareTrainingAndTestTree(ROOT.TCut(''),
#        # NOTE: Use this config if you run on the full dataset
#        #'TrainTestSplit_quarks=0.8:TrainTestSplit_gluons=0.8:'+\
#        'nTrain_Signal=900:nTest_Signal=900:'+\
#        'nTrain_Background=900:nTest_Background=900:'+\
#        'SplitMode=Random:NormMode=NumEvents:!V')


# In[4]:

#ml dataloader.DrawInputVariable("ntracks")


# ## Book MVA methods

# In[5]:

#LD
factory.BookMethod(dataloader, ROOT.TMVA.Types.kLD, "LD", 
    "H:!V:VarTransform=None:CreateMVAPdfs=True:PDFInterpolMVAPdf=Spline2:NbinsMVAPdf=50:NsmoothMVAPdf=10" )

# Fisher's discriminant
factory.BookMethod(dataloader, ROOT.TMVA.Types.kFisher, 'Fisher',
        'H:!V:VarTransform=None:Fisher')

# k-Nearest Neighbors
factory.BookMethod(dataloader, ROOT.TMVA.Types.kKNN, 'KNN',
        'H:!V:VarTransform=None:nkNN=5')

# Boosted Decision Tree
factory.BookMethod(dataloader, ROOT.TMVA.Types.kBDT, 'BDT',
        'H:!V:VarTransform=None:NTrees=100:MaxDepth=2:nCuts=10')


# **The following method PyKeras does only work with the software stack 88 or the bleeding edge software stack of SWAN!**
# 
# Please note, that running this notebook the first time can take some time because Theano compiles code in the background.

# In[6]:

# Set Theano as backend of Keras
from os import environ
environ['KERAS_BACKEND'] = 'theano'

# Set architecture of system (AVX instruction set is not supported on SWAN and lxplus)
environ['THEANO_FLAGS'] = 'gcc.cxxflags=-march=corei7'

from keras.models import Sequential
from keras.layers.core import Dense
from keras.optimizers import SGD

# Initialize PyMVA which implements the interface for Keras
ROOT.TMVA.PyMethodBase.PyInitialize()

# Set up neural network architecture
model = Sequential()
model.add(Dense(64, init='glorot_normal', activation='relu',
        input_dim=dataloader.GetDataSetInfo().GetNVariables()))
model.add(Dense(64, init='glorot_normal', activation='relu'))
model.add(Dense(2, init='glorot_uniform', activation='softmax'))

# Define loss, optimizer and validation metrics
model.compile(loss='categorical_crossentropy',
        optimizer=SGD(lr=1e-2),
        metrics=['accuracy',])

# Save model and print summary of set up architecture
filename_model = 'PyKerasModel.h5'
model.save(filename_model)
model.summary()

# Book method
factory.BookMethod(dataloader, ROOT.TMVA.Types.kPyKeras, 'PyKeras',
        'H:!V:VarTransform=G:FilenameModel={}:'.format(filename_model)+\
        'NumEpochs=5:BatchSize=100:SaveBestOnly=True:TriesEarlyStopping=1')


# ## Train, test and evaluate methods

# In[7]:

factory.TrainAllMethods()


# In[8]:

factory.TestAllMethods()


# In[9]:

factory.EvaluateAllMethods()


# ## Get results
# 
# In the following, we are plotting the receiver operating characteristic, which integral - the so called area under curve or ROC integral - is the figure of merit for this challenge. The explicit value is read out below and can be extracted from the TMVA evaluation output above as well.
# 
# As well, this example comes with a script `RUN_TMVA_GUI`, which can be executed from any terminal with connection to a graphical interface. It will run ROOT and open the full TMVA evaluation with all information taken from the file `TMVAClassification.root`, that is created by running this notebook.

# In[10]:

# Plot ROC

# Enable JavaScript magic
#get_ipython().magic(u'jsroot on')

canvas = factory.GetROCCurve(dataloader)
canvas.Draw()


# In[11]:

# Print area-under-curve (ROC integral) for Fisher method
print('AUC: {0}'.format(factory.GetROCIntegral(dataloader, 'Fisher')))


# # Preparing the results submission
# 
# Please use the TMVAMeasureAUC.ipynb notebook to evaluate the AUC in the "modified" data set (for the results submission).

# In[ ]:



