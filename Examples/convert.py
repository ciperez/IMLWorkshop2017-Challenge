import flat_helper
import pandas as pd
NEVENTS = -1
BASEPATH = '/eos/project/i/iml/IMLChallengeQG'
OUTPUTPATH = '/eos/user/k/kreczko/SWAN_projects/IMLWorkshop2017-Challenge/Results'

dfQuarks = flat_helper.GetJetShapes(BASEPATH + '/quarks_standard/*.root', NEVENTS, recompute=True)
dfGluons = flat_helper.GetJetShapes(BASEPATH +'/gluons_standard/*.root', NEVENTS, recompute=True)
dfQuarks['isGluon'] = 0
dfGluons['isGluon'] = 1

data = pd.concat([dfQuarks, dfGluons]).reset_index(drop=True)
data.to_csv(OUTPUTPATH + '/train.csv')

del data, dfQuarks, dfGluons

dfQuarks = flat_helper.GetJetShapes(BASEPATH + '/quarks_modified/*.root', NEVENTS, recompute=True)
dfGluons = flat_helper.GetJetShapes(BASEPATH + '/gluons_modified/*.root', NEVENTS, recompute=True)
dfQuarks['isGluon'] = 0
dfGluons['isGluon'] = 1

data = pd.concat([dfQuarks, dfGluons]).reset_index(drop=True)
data.to_csv(OUTPUTPATH + '/test.csv')
