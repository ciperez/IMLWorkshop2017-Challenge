﻿using ChallengeData;
using LINQToTreeHelpers.FutureUtils;
using SimpleTMVATraining.utils;
using System.IO;
using System.Linq;
using TMVAUtilities;
using static SimpleTMVATraining.utils.FileUtils;
using System;
using System.Linq.Expressions;
using LinqToTTreeInterfacesLib;
using LINQToTTreeLib;
using LINQToTreeHelpers;
using ROOTNET.Interface;

namespace SimpleTMVATraining
{
    class Program
    {
        /// <summary>
        /// Invoke with the first argument the directory where the folders containing the data files are located.
        /// </summary>
        /// <param name="args">Command line arguments</param>
        /// <remarks>
        /// Preparing the machine for running. This was done on Win10, but Win8 or Win7 should work.
        /// Since this isn't SWAN or similar, some config needs to be done. :( :(
        /// 1) Install free Visual Studio Community edition 2013 (you only need the C++ compiler)
        /// 2) Install free Visual Studio Community edition 2015
        /// 3) Install 7-Zip (to make sure that the correct version of root can be downloaded and unpacked)
        /// 4) Checkout this project from git
        /// 5) Double-click on the .sln file to start it up
        /// 6) Right click and click build. This will take a while - likely it will download ROOT and a bunch of
        ///    other packages from nuget.org and myget.org
        /// 7) Exit and double click on the "start_xxx.bat" file. This will make sure ROOT env vars are defined.
        /// 8) Download the data
        /// 9) Add the top level directory where the data is located as a command line argument.
        /// 9) Run.
        /// 
        /// Never had time to optimize or even add more interesting variables. Jetlag....
        /// </remarks>
        static void Main(string[] args)
        {
            // Get the files that we are going to be accessing.
            var files = DataFiles(new DirectoryInfo(args[0]));

            // Next, get our training datasets
            var trainingEvents_gluons = makeTrainingDataset(files._gluon_standard);
            var trainingEvents_quarks = makeTrainingDataset(files._quark_standard);

            // Make some standard plots, and save them to a root file.
            using (var output = new FutureTFile("SimpleTMVATraining.root"))
            {
                trainingEvents_gluons.MakeStandardPlots(output.mkdir("gluons_standard"));
                trainingEvents_quarks.MakeStandardPlots(output.mkdir("quarks_standard"));

                // Now, build the flat ntuple that will become our training data.
                // This includes building other high-level objects
                var training_gluons = MakeTrainingData(trainingEvents_gluons);
                var training_quarks = MakeTrainingData(trainingEvents_quarks);

                // Setup the training.
                var training = training_quarks.AsSignal("quarks")
                    .Background(training_gluons);
                var bdt = training.AddMethod(ROOTNET.Interface.NTMVA.NTypes.EMVA.kBDT, "BDT");

                training.Train("MasterTraining");

                // Next, create the ROC curve on the standard sample
                var ROCArea = BuildRockCurve(bdt, training_quarks, training_gluons, output.mkdir("bdt_output"));

                // Do it for the modified background
                var testing_gluons = MakeTrainingData(makeTrainingDataset(files._gluon_modified));
                var testing_quarks = MakeTrainingData(makeTrainingDataset(files._quark_modified));
                var ROCModified = BuildRockCurve(bdt, testing_quarks, testing_gluons, output.mkdir("bdt_modified"));

                // Dump everything
                WriteROCInfo(ROCArea, "_standard");
                WriteROCInfo(ROCModified, "");
            }
        }

        /// <summary>
        /// Write out the challenge file.
        /// </summary>
        /// <param name="rocValue"></param>
        /// <param name="name"></param>
        private static void WriteROCInfo(IFutureValue<double> rocValue, string name)
        {
            Console.WriteLine($"Area under ROC for standard sample curve: {rocValue.Value}");
            using (var writer = File.CreateText($"..\\..\\..\\..\\..\\gwatts_v0_auc{name}.txt"))
            {
                writer.WriteLine(rocValue.Value);
            }
        }

        /// <summary>
        /// Create the plots for a ROC curve
        /// </summary>
        /// <param name="bdt"></param>
        /// <param name="training_quarks"></param>
        /// <param name="training_gluons"></param>
        private static IFutureValue<double> BuildRockCurve(Method<TrainingData> bdt, IQueryable<TrainingData> training_quarks, IQueryable<TrainingData> training_gluons,
            FutureTDirectory output)
        {
            var cBDT = bdt.GetMVAValue();

            // Generate a highly granulated mva plot we can then use to genrate the ROC.
            var hq = PlotMVAResult(cBDT, training_quarks, "quarks")
                .Save(output);
            var hg = PlotMVAResult(cBDT, training_gluons, "gluons")
                .Save(output);

            var roc = from g in hg from q in hq select utils.PlottingUtils.CalculateROC(q, g);
            roc.Save(output);

            return from r in roc select utils.PlottingUtils.CalculateAUC(r);
        }

        /// <summary>
        /// Plot the mva result.
        /// </summary>
        /// <param name="cBDT"></param>
        /// <param name="sample"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        private static IFutureValue<ROOTNET.Interface.NTH1> PlotMVAResult(Expression<Func<TrainingData, double>> cBDT, IQueryable<TrainingData> sample, string name)
        {
            var h = sample
                .Select(t => cBDT.Invoke(t))
                .FuturePlot(utils.PlottingUtils.bdtOutput, name);

            return h;
        }

        const double TrackJetDRSquared = 0.2 * 0.2;

        /// <summary>
        /// Convert the raw ntuple into a flat ntuple that has just the training data.
        /// The high level features are calculated here.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static IQueryable<TrainingData> MakeTrainingData (IQueryable<treeJets> source)
        {
            return source
                .Select(e => new TrainingData()
                {
                    Weight = 1.0,
                    jetPt = e.jetPt,
                    //nTracks = (from t in e.tracks where t.pt > 2.0 && ROOTUtils.DeltaR2(e.jetEta, e.jetPhi, t.eta, t.phi) < TrackJetDRSquared
                    //          select t).Count()
                    nTracks = e.ntracks,
                    nTowers = e.ntowers,
                });
        }

        /// <summary>
        /// List of all the variables we will feed to the training folks.
        /// </summary>
        public class TrainingData
        {
            /// <summary>
            /// Weight for each jet
            /// </summary>
            public double Weight;

            /// <summary>
            /// The pt of the jet
            /// </summary>
            public double jetPt;

            /// <summary>
            /// Number of tracks near the jet axis
            /// </summary>
            public double nTracks;

            /// <summary>
            /// How many towers. A proxy for width
            /// </summary>
            public double nTowers;
        }
    }
}
