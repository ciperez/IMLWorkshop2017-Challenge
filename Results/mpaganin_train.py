import os
import matplotlib.pyplot as plt
import numpy as np
import deepdish.io as io
from keras.callbacks import EarlyStopping, ModelCheckpoint
#from keras.utils.vis_utils import plot_model as plot
from sklearn.metrics import roc_curve, roc_auc_score

from models import fancy_rnn

weights_file = 'attempt0322.h5'

print 'Loading train set'
train_dict = io.load(os.path.join('data', 'standard.h5'))
for k, v in train_dict.iteritems():
    train_dict[k] = v[:100000]
print 'Loading test set'
#test_dict = io.load(os.path.join('data', 'modified.h5'))
test_dict = train_dict
net = twornn_flat(train_dict)
net.summary()
net.compile('adam', 'binary_crossentropy')

#plot(net, to_file='model.png', show_shapes=True)

try:
	net.load_weights(weights_file)
	print 'Weights loaded'
except IOError:
	print 'Weights not found'

print 'Training:'
try:
    net.fit(
        [train_dict['X_tracks'], train_dict['X_towers'], train_dict['X_flat']],
        train_dict['y'],
        batch_size=128, 
            class_weight={ # classes are unbalanced, consider balancing them
                0 : 0.5 * (float(len(train_dict['y'])) / (train_dict['y'] == 0).sum()),
                1 : 0.5 * (float(len(train_dict['y'])) / (train_dict['y'] == 1).sum())
        },
        callbacks = [
            EarlyStopping(verbose=True, patience=20, monitor='val_loss'),
            ModelCheckpoint(weights_file, monitor='val_loss', verbose=True, save_best_only=True)
        ],
    nb_epoch=100, 
        validation_split = 0.3) 

except KeyboardInterrupt:
    print 'Training ended early.'

print 'Loading best networks weights'
net.load_weights(weights_file)

yhat = net.predict([test_dict['X_tracks'], test_dict['X_towers'], test_dict['X_flat']],
                   verbose=True, batch_size=10000
)

# -- plot classifier distribution
#bins = np.linspace(0, 1, 20)
#plt.figure(figsize=(10, 10))
#_ = plt.hist(yhat[test_dict['y'] == 0], bins=bins, histtype='step', label='Quarks')
#_ = plt.hist(yhat[test_dict['y'] == 1], bins=bins, histtype='step', label='Gluons')
#plt.legend()
#plt.savefig('classifier_output.png')

print roc_auc_score(test_dict['y'], yhat)
